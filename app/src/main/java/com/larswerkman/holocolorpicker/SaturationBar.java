package com.larswerkman.holocolorpicker;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.paint.fw.R;

public class SaturationBar extends View {
   private static final boolean ORIENTATION_DEFAULT = true;
   private static final boolean ORIENTATION_HORIZONTAL = true;
   private static final boolean ORIENTATION_VERTICAL = false;
   private static final String STATE_COLOR = "color";
   private static final String STATE_ORIENTATION = "orientation";
   private static final String STATE_PARENT = "parent";
   private static final String STATE_SATURATION = "saturation";
   private int mBarLength;
   private Paint mBarPaint;
   private Paint mBarPointerHaloPaint;
   private int mBarPointerHaloRadius;
   private Paint mBarPointerPaint;
   private int mBarPointerPosition;
   private int mBarPointerRadius;
   private RectF mBarRect = new RectF();
   private int mBarThickness;
   private int mColor;
   private float[] mHSVColor = new float[3];
   private boolean mIsMovingPointer;
   private boolean mOrientation;
   private ColorPicker mPicker = null;
   private float mPosToSatFactor;
   private int mPreferredBarLength;
   private float mSatToPosFactor;
   private int oldChangedListenerSaturation;
   private SaturationBar.OnSaturationChangedListener onSaturationChangedListener;
   private Shader shader;

   public SaturationBar(Context var1) {
      super(var1);
      this.init((AttributeSet) null, 0);
   }

   public SaturationBar(Context var1, AttributeSet var2) {
      super(var1, var2);
      this.init(var2, 0);
   }

   public SaturationBar(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
      this.init(var2, var3);
   }

   private void calculateColor(int var1) {
      int var2 = var1 - this.mBarPointerHaloRadius;
      if(var2 < 0) {
         var1 = 0;
      } else {
         var1 = var2;
         if(var2 > this.mBarLength) {
            var1 = this.mBarLength;
         }
      }

      this.mColor = Color.HSVToColor(new float[]{this.mHSVColor[0], this.mPosToSatFactor * (float)var1, 1.0F});
   }

   private void init(AttributeSet var1, int var2) {
      TypedArray var4 = this.getContext().obtainStyledAttributes(var1, RR.styleable.ColorBars, var2, 0);
      Resources var3 = this.getContext().getResources();
      this.mBarThickness = var4.getDimensionPixelSize(0, var3.getDimensionPixelSize(R.dimen.bar_thickness));
      this.mBarLength = var4.getDimensionPixelSize(1, var3.getDimensionPixelSize(R.dimen.bar_length));
      this.mPreferredBarLength = this.mBarLength;
      this.mBarPointerRadius = var4.getDimensionPixelSize(2, var3.getDimensionPixelSize(R.dimen.bar_pointer_radius));
      this.mBarPointerHaloRadius = var4.getDimensionPixelSize(3, var3.getDimensionPixelSize(R.dimen.bar_pointer_halo_radius));
      this.mOrientation = var4.getBoolean(4, true);
      var4.recycle();
      this.mBarPaint = new Paint(1);
      this.mBarPaint.setShader(this.shader);
      this.mBarPointerPosition = this.mBarLength + this.mBarPointerHaloRadius;
      this.mBarPointerHaloPaint = new Paint(1);
      this.mBarPointerHaloPaint.setColor(-16777216);
      this.mBarPointerHaloPaint.setAlpha(80);
      this.mBarPointerPaint = new Paint(1);
      this.mBarPointerPaint.setColor(-8257792);
      this.mPosToSatFactor = 1.0F / (float)this.mBarLength;
      this.mSatToPosFactor = (float)this.mBarLength / 1.0F;
   }

   public int getColor() {
      return this.mColor;
   }

   public SaturationBar.OnSaturationChangedListener getOnSaturationChangedListener() {
      return this.onSaturationChangedListener;
   }

   protected void onDraw(Canvas var1) {
      var1.drawRect(this.mBarRect, this.mBarPaint);
      int var2;
      int var3;
      if(this.mOrientation) {
         var3 = this.mBarPointerPosition;
         var2 = this.mBarPointerHaloRadius;
      } else {
         var3 = this.mBarPointerHaloRadius;
         var2 = this.mBarPointerPosition;
      }

      var1.drawCircle((float)var3, (float)var2, (float)this.mBarPointerHaloRadius, this.mBarPointerHaloPaint);
      var1.drawCircle((float)var3, (float)var2, (float)this.mBarPointerRadius, this.mBarPointerPaint);
   }

   protected void onMeasure(int var1, int var2) {
      int var3 = this.mPreferredBarLength + this.mBarPointerHaloRadius * 2;
      if(!this.mOrientation) {
         var1 = var2;
      }

      var2 = MeasureSpec.getMode(var1);
      var1 = MeasureSpec.getSize(var1);
      if(var2 != 1073741824) {
         if(var2 == Integer.MIN_VALUE) {
            var1 = Math.min(var3, var1);
         } else {
            var1 = var3;
         }
      }

      var2 = this.mBarPointerHaloRadius * 2;
      this.mBarLength = var1 - var2;
      if(!this.mOrientation) {
         this.setMeasuredDimension(var2, this.mBarLength + var2);
      } else {
         this.setMeasuredDimension(this.mBarLength + var2, var2);
      }

   }

   protected void onRestoreInstanceState(Parcelable var1) {
      Bundle var2 = (Bundle)var1;
      super.onRestoreInstanceState(var2.getParcelable("parent"));
      this.setColor(Color.HSVToColor(var2.getFloatArray("color")));
      this.setSaturation(var2.getFloat("saturation"));
      this.mOrientation = var2.getBoolean("orientation", true);
   }

   protected Parcelable onSaveInstanceState() {
      Parcelable var2 = super.onSaveInstanceState();
      Bundle var1 = new Bundle();
      var1.putParcelable("parent", var2);
      var1.putFloatArray("color", this.mHSVColor);
      var1.putBoolean("orientation", this.mOrientation);
      float[] var3 = new float[3];
      Color.colorToHSV(this.mColor, var3);
      var1.putFloat("saturation", var3[1]);
      return var1;
   }

   protected void onSizeChanged(int var1, int var2, int var3, int var4) {
      super.onSizeChanged(var1, var2, var3, var4);
      if(this.mOrientation) {
         var3 = this.mBarLength + this.mBarPointerHaloRadius;
         var2 = this.mBarThickness;
         this.mBarLength = var1 - this.mBarPointerHaloRadius * 2;
         this.mBarRect.set((float)this.mBarPointerHaloRadius, (float)(this.mBarPointerHaloRadius - this.mBarThickness / 2), (float)(this.mBarLength + this.mBarPointerHaloRadius), (float)(this.mBarPointerHaloRadius + this.mBarThickness / 2));
         var1 = var3;
      } else {
         var1 = this.mBarThickness;
         var3 = this.mBarLength + this.mBarPointerHaloRadius;
         this.mBarLength = var2 - this.mBarPointerHaloRadius * 2;
         this.mBarRect.set((float)this.mBarPointerHaloRadius, (float)(this.mBarPointerHaloRadius - this.mBarThickness / 2), (float)(this.mBarPointerHaloRadius + this.mBarThickness / 2), (float)(this.mBarLength + this.mBarPointerHaloRadius));
         var2 = var3;
      }

      float var5;
      float var6;
      float var7;
      TileMode var8;
      if(!this.isInEditMode()) {
         var6 = (float)this.mBarPointerHaloRadius;
         var7 = (float)var1;
         var5 = (float)var2;
         var1 = Color.HSVToColor(255, this.mHSVColor);
         var8 = TileMode.CLAMP;
         this.shader = new LinearGradient(var6, 0.0F, var7, var5, new int[]{-1, var1}, (float[])null, var8);
      } else {
         var7 = (float)this.mBarPointerHaloRadius;
         var5 = (float)var1;
         var6 = (float)var2;
         var8 = TileMode.CLAMP;
         this.shader = new LinearGradient(var7, 0.0F, var5, var6, new int[]{-1, -8257792}, (float[])null, var8);
         Color.colorToHSV(-8257792, this.mHSVColor);
      }

      this.mBarPaint.setShader(this.shader);
      this.mPosToSatFactor = 1.0F / (float)this.mBarLength;
      this.mSatToPosFactor = (float)this.mBarLength / 1.0F;
      float[] var9 = new float[3];
      Color.colorToHSV(this.mColor, var9);
      if(!this.isInEditMode()) {
         this.mBarPointerPosition = Math.round(this.mSatToPosFactor * var9[1] + (float)this.mBarPointerHaloRadius);
      } else {
         this.mBarPointerPosition = this.mBarLength + this.mBarPointerHaloRadius;
      }

   }

   public boolean onTouchEvent(MotionEvent var1) {
      this.getParent().requestDisallowInterceptTouchEvent(true);
      float var2;
      if(this.mOrientation) {
         var2 = var1.getX();
      } else {
         var2 = var1.getY();
      }

      switch(var1.getAction()) {
      case 0:
         this.mIsMovingPointer = true;
         if(var2 >= (float)this.mBarPointerHaloRadius && var2 <= (float)(this.mBarPointerHaloRadius + this.mBarLength)) {
            this.mBarPointerPosition = Math.round(var2);
            this.calculateColor(Math.round(var2));
            this.mBarPointerPaint.setColor(this.mColor);
            this.invalidate();
         }
         break;
      case 1:
         this.mIsMovingPointer = false;
         break;
      case 2:
         if(this.mIsMovingPointer) {
            if(var2 >= (float)this.mBarPointerHaloRadius && var2 <= (float)(this.mBarPointerHaloRadius + this.mBarLength)) {
               this.mBarPointerPosition = Math.round(var2);
               this.calculateColor(Math.round(var2));
               this.mBarPointerPaint.setColor(this.mColor);
               if(this.mPicker != null) {
                  this.mPicker.setNewCenterColor(this.mColor);
                  this.mPicker.changeValueBarColor(this.mColor);
                  this.mPicker.changeOpacityBarColor(this.mColor);
               }

               this.invalidate();
            } else if(var2 < (float)this.mBarPointerHaloRadius) {
               this.mBarPointerPosition = this.mBarPointerHaloRadius;
               this.mColor = -1;
               this.mBarPointerPaint.setColor(this.mColor);
               if(this.mPicker != null) {
                  this.mPicker.setNewCenterColor(this.mColor);
                  this.mPicker.changeValueBarColor(this.mColor);
                  this.mPicker.changeOpacityBarColor(this.mColor);
               }

               this.invalidate();
            } else if(var2 > (float)(this.mBarPointerHaloRadius + this.mBarLength)) {
               this.mBarPointerPosition = this.mBarPointerHaloRadius + this.mBarLength;
               this.mColor = Color.HSVToColor(this.mHSVColor);
               this.mBarPointerPaint.setColor(this.mColor);
               if(this.mPicker != null) {
                  this.mPicker.setNewCenterColor(this.mColor);
                  this.mPicker.changeValueBarColor(this.mColor);
                  this.mPicker.changeOpacityBarColor(this.mColor);
               }

               this.invalidate();
            }
         }

         if(this.onSaturationChangedListener != null && this.oldChangedListenerSaturation != this.mColor) {
            this.onSaturationChangedListener.onSaturationChanged(this.mColor);
            this.oldChangedListenerSaturation = this.mColor;
         }
      }

      return true;
   }

   public void setColor(int var1) {
      int var5;
      int var6;
      if(this.mOrientation) {
         var6 = this.mBarLength + this.mBarPointerHaloRadius;
         var5 = this.mBarThickness;
      } else {
         var6 = this.mBarThickness;
         var5 = this.mBarLength + this.mBarPointerHaloRadius;
      }

      Color.colorToHSV(var1, this.mHSVColor);
      float var3 = (float)this.mBarPointerHaloRadius;
      float var4 = (float)var6;
      float var2 = (float)var5;
      TileMode var7 = TileMode.CLAMP;
      this.shader = new LinearGradient(var3, 0.0F, var4, var2, new int[]{-1, var1}, (float[])null, var7);
      this.mBarPaint.setShader(this.shader);
      this.calculateColor(this.mBarPointerPosition);
      this.mBarPointerPaint.setColor(this.mColor);
      if(this.mPicker != null) {
         this.mPicker.setNewCenterColor(this.mColor);
         if(this.mPicker.hasValueBar()) {
            this.mPicker.changeValueBarColor(this.mColor);
         } else if(this.mPicker.hasOpacityBar()) {
            this.mPicker.changeOpacityBarColor(this.mColor);
         }
      }

      this.invalidate();
   }

   public void setColorPicker(ColorPicker var1) {
      this.mPicker = var1;
   }

   public void setOnSaturationChangedListener(SaturationBar.OnSaturationChangedListener var1) {
      this.onSaturationChangedListener = var1;
   }

   public void setSaturation(float var1) {
      this.mBarPointerPosition = Math.round(this.mSatToPosFactor * var1) + this.mBarPointerHaloRadius;
      this.calculateColor(this.mBarPointerPosition);
      this.mBarPointerPaint.setColor(this.mColor);
      if(this.mPicker != null) {
         this.mPicker.setNewCenterColor(this.mColor);
         this.mPicker.changeValueBarColor(this.mColor);
         this.mPicker.changeOpacityBarColor(this.mColor);
      }

      this.invalidate();
   }

   public interface OnSaturationChangedListener {
      void onSaturationChanged(int var1);
   }
}
