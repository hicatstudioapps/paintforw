package com.larswerkman.holocolorpicker;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.SweepGradient;
import android.graphics.Paint.Style;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import com.larswerkman.holocolorpicker.OpacityBar;
import com.larswerkman.holocolorpicker.SVBar;
import com.larswerkman.holocolorpicker.SaturationBar;
import com.larswerkman.holocolorpicker.ValueBar;
import com.paint.fw.R;

public class ColorPicker extends View {
   private static final int[] COLORS = new int[]{-65536, -65281, -16776961, -16711681, -16711936, -256, -65536};
   private static final String STATE_ANGLE = "angle";
   private static final String STATE_OLD_COLOR = "color";
   private static final String STATE_PARENT = "parent";
   private static final String STATE_SHOW_OLD_COLOR = "showColor";
   private float mAngle;
   private Paint mCenterHaloPaint;
   private int mCenterNewColor;
   private Paint mCenterNewPaint;
   private int mCenterOldColor;
   private Paint mCenterOldPaint;
   private RectF mCenterRectangle = new RectF();
   private int mColor;
   private int mColorCenterHaloRadius;
   private int mColorCenterRadius;
   private int mColorPointerHaloRadius;
   private int mColorPointerRadius;
   private Paint mColorWheelPaint;
   private int mColorWheelRadius;
   private RectF mColorWheelRectangle = new RectF();
   private int mColorWheelThickness;
   private float[] mHSV = new float[3];
   private OpacityBar mOpacityBar = null;
   private Paint mPointerColor;
   private Paint mPointerHaloPaint;
   private int mPreferredColorCenterHaloRadius;
   private int mPreferredColorCenterRadius;
   private int mPreferredColorWheelRadius;
   private SVBar mSVbar = null;
   private SaturationBar mSaturationBar = null;
   private boolean mShowCenterOldColor;
   private float mSlopX;
   private float mSlopY;
   private float mTranslationOffset;
   private boolean mUserIsMovingPointer = false;
   private ValueBar mValueBar = null;
   private int oldChangedListenerColor;
   private int oldSelectedListenerColor;
   private ColorPicker.OnColorChangedListener onColorChangedListener;
   private ColorPicker.OnColorSelectedListener onColorSelectedListener;

   public ColorPicker(Context var1) {
      super(var1);
      this.init((AttributeSet)null, 0);
   }

   public ColorPicker(Context var1, AttributeSet var2) {
      super(var1, var2);
      this.init(var2, 0);
   }

   public ColorPicker(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
      this.init(var2, var3);
   }

   private int ave(int var1, int var2, float var3) {
      return Math.round((float)(var2 - var1) * var3) + var1;
   }

   private int calculateColor(float var1) {
      float var2 = (float)((double)var1 / 6.283185307179586D);
      var1 = var2;
      if(var2 < 0.0F) {
         var1 = var2 + 1.0F;
      }

      int var3;
      if(var1 <= 0.0F) {
         this.mColor = COLORS[0];
         var3 = COLORS[0];
      } else if(var1 >= 1.0F) {
         this.mColor = COLORS[COLORS.length - 1];
         var3 = COLORS[COLORS.length - 1];
      } else {
         var1 *= (float)(COLORS.length - 1);
         int var4 = (int)var1;
         var1 -= (float)var4;
         var3 = COLORS[var4];
         int var7 = COLORS[var4 + 1];
         var4 = this.ave(Color.alpha(var3), Color.alpha(var7), var1);
         int var6 = this.ave(Color.red(var3), Color.red(var7), var1);
         int var5 = this.ave(Color.green(var3), Color.green(var7), var1);
         var3 = this.ave(Color.blue(var3), Color.blue(var7), var1);
         this.mColor = Color.argb(var4, var6, var5, var3);
         var3 = Color.argb(var4, var6, var5, var3);
      }

      return var3;
   }

   private float[] calculatePointerPosition(float var1) {
      return new float[]{(float)((double)this.mColorWheelRadius * Math.cos((double)var1)), (float)((double)this.mColorWheelRadius * Math.sin((double)var1))};
   }

   private float colorToAngle(int var1) {
      float[] var2 = new float[3];
      Color.colorToHSV(var1, var2);
      return (float)Math.toRadians((double)(-var2[0]));
   }

   private void init(AttributeSet var1, int var2) {
      TypedArray var3 = this.getContext().obtainStyledAttributes(var1, RR.styleable.ColorPicker, var2, 0);
      Resources var4 = this.getContext().getResources();
      this.mColorWheelThickness = var3.getDimensionPixelSize(1, var4.getDimensionPixelSize(R.dimen.color_wheel_thickness));
      this.mColorWheelRadius = var3.getDimensionPixelSize(0, var4.getDimensionPixelSize(R.dimen.color_wheel_radius));
      this.mPreferredColorWheelRadius = this.mColorWheelRadius;
      this.mColorCenterRadius = var3.getDimensionPixelSize(2, var4.getDimensionPixelSize(R.dimen.color_center_radius));
      this.mPreferredColorCenterRadius = this.mColorCenterRadius;
      this.mColorCenterHaloRadius = var3.getDimensionPixelSize(3, var4.getDimensionPixelSize(R.dimen.color_center_halo_radius));
      this.mPreferredColorCenterHaloRadius = this.mColorCenterHaloRadius;
      this.mColorPointerRadius = var3.getDimensionPixelSize(4, var4.getDimensionPixelSize(R.dimen.color_pointer_radius));
      this.mColorPointerHaloRadius = var3.getDimensionPixelSize(5, var4.getDimensionPixelSize(R.dimen.color_pointer_halo_radius));
      var3.recycle();
      this.mAngle = -1.5707964F;
      SweepGradient var5 = new SweepGradient(0.0F, 0.0F, COLORS, (float[])null);
      this.mColorWheelPaint = new Paint(1);
      this.mColorWheelPaint.setShader(var5);
      this.mColorWheelPaint.setStyle(Style.STROKE);
      this.mColorWheelPaint.setStrokeWidth((float)this.mColorWheelThickness);
      this.mPointerHaloPaint = new Paint(1);
      this.mPointerHaloPaint.setColor(-16777216);
      this.mPointerHaloPaint.setAlpha(80);
      this.mPointerColor = new Paint(1);
      this.mPointerColor.setColor(this.calculateColor(this.mAngle));
      this.mCenterNewPaint = new Paint(1);
      this.mCenterNewPaint.setColor(this.calculateColor(this.mAngle));
      this.mCenterNewPaint.setStyle(Style.FILL);
      this.mCenterOldPaint = new Paint(1);
      this.mCenterOldPaint.setColor(this.calculateColor(this.mAngle));
      this.mCenterOldPaint.setStyle(Style.FILL);
      this.mCenterHaloPaint = new Paint(1);
      this.mCenterHaloPaint.setColor(-16777216);
      this.mCenterHaloPaint.setAlpha(0);
      this.mCenterNewColor = this.calculateColor(this.mAngle);
      this.mCenterOldColor = this.calculateColor(this.mAngle);
      this.mShowCenterOldColor = true;
   }

   public void addOpacityBar(OpacityBar var1) {
      this.mOpacityBar = var1;
      this.mOpacityBar.setColorPicker(this);
      this.mOpacityBar.setColor(this.mColor);
   }

   public void addSVBar(SVBar var1) {
      this.mSVbar = var1;
      this.mSVbar.setColorPicker(this);
      this.mSVbar.setColor(this.mColor);
   }

   public void addSaturationBar(SaturationBar var1) {
      this.mSaturationBar = var1;
      this.mSaturationBar.setColorPicker(this);
      this.mSaturationBar.setColor(this.mColor);
   }

   public void addValueBar(ValueBar var1) {
      this.mValueBar = var1;
      this.mValueBar.setColorPicker(this);
      this.mValueBar.setColor(this.mColor);
   }

   public void changeOpacityBarColor(int var1) {
      if(this.mOpacityBar != null) {
         this.mOpacityBar.setColor(var1);
      }

   }

   public void changeSaturationBarColor(int var1) {
      if(this.mSaturationBar != null) {
         this.mSaturationBar.setColor(var1);
      }

   }

   public void changeValueBarColor(int var1) {
      if(this.mValueBar != null) {
         this.mValueBar.setColor(var1);
      }

   }

   public int getColor() {
      return this.mCenterNewColor;
   }

   public int getOldCenterColor() {
      return this.mCenterOldColor;
   }

   public ColorPicker.OnColorChangedListener getOnColorChangedListener() {
      return this.onColorChangedListener;
   }

   public ColorPicker.OnColorSelectedListener getOnColorSelectedListener() {
      return this.onColorSelectedListener;
   }

   public boolean getShowOldCenterColor() {
      return this.mShowCenterOldColor;
   }

   public boolean hasOpacityBar() {
      boolean var1;
      if(this.mOpacityBar != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean hasSVBar() {
      boolean var1;
      if(this.mSVbar != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean hasSaturationBar() {
      boolean var1;
      if(this.mSaturationBar != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean hasValueBar() {
      boolean var1;
      if(this.mValueBar != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   protected void onDraw(Canvas var1) {
      var1.translate(this.mTranslationOffset, this.mTranslationOffset);
      var1.drawOval(this.mColorWheelRectangle, this.mColorWheelPaint);
      float[] var2 = this.calculatePointerPosition(this.mAngle);
      var1.drawCircle(var2[0], var2[1], (float)this.mColorPointerHaloRadius, this.mPointerHaloPaint);
      var1.drawCircle(var2[0], var2[1], (float)this.mColorPointerRadius, this.mPointerColor);
      var1.drawCircle(0.0F, 0.0F, (float)this.mColorCenterHaloRadius, this.mCenterHaloPaint);
      if(this.mShowCenterOldColor) {
         var1.drawArc(this.mCenterRectangle, 90.0F, 180.0F, true, this.mCenterOldPaint);
         var1.drawArc(this.mCenterRectangle, 270.0F, 180.0F, true, this.mCenterNewPaint);
      } else {
         var1.drawArc(this.mCenterRectangle, 0.0F, 360.0F, true, this.mCenterNewPaint);
      }

   }

   protected void onMeasure(int var1, int var2) {
      int var3 = (this.mPreferredColorWheelRadius + this.mColorPointerHaloRadius) * 2;
      int var5 = MeasureSpec.getMode(var1);
      var1 = MeasureSpec.getSize(var1);
      int var4 = MeasureSpec.getMode(var2);
      var2 = MeasureSpec.getSize(var2);
      if(var5 != 1073741824) {
         if(var5 == Integer.MIN_VALUE) {
            var1 = Math.min(var3, var1);
         } else {
            var1 = var3;
         }
      }

      if(var4 != 1073741824) {
         if(var4 == Integer.MIN_VALUE) {
            var2 = Math.min(var3, var2);
         } else {
            var2 = var3;
         }
      }

      var1 = Math.min(var1, var2);
      this.setMeasuredDimension(var1, var1);
      this.mTranslationOffset = (float)var1 * 0.5F;
      this.mColorWheelRadius = var1 / 2 - this.mColorWheelThickness - this.mColorPointerHaloRadius;
      this.mColorWheelRectangle.set((float)(-this.mColorWheelRadius), (float)(-this.mColorWheelRadius), (float)this.mColorWheelRadius, (float)this.mColorWheelRadius);
      this.mColorCenterRadius = (int)((float)this.mPreferredColorCenterRadius * ((float)this.mColorWheelRadius / (float)this.mPreferredColorWheelRadius));
      this.mColorCenterHaloRadius = (int)((float)this.mPreferredColorCenterHaloRadius * ((float)this.mColorWheelRadius / (float)this.mPreferredColorWheelRadius));
      this.mCenterRectangle.set((float)(-this.mColorCenterRadius), (float)(-this.mColorCenterRadius), (float)this.mColorCenterRadius, (float)this.mColorCenterRadius);
   }

   protected void onRestoreInstanceState(Parcelable var1) {
      Bundle var3 = (Bundle)var1;
      super.onRestoreInstanceState(var3.getParcelable("parent"));
      this.mAngle = var3.getFloat("angle");
      this.setOldCenterColor(var3.getInt("color"));
      this.mShowCenterOldColor = var3.getBoolean("showColor");
      int var2 = this.calculateColor(this.mAngle);
      this.mPointerColor.setColor(var2);
      this.setNewCenterColor(var2);
   }

   protected Parcelable onSaveInstanceState() {
      Parcelable var1 = super.onSaveInstanceState();
      Bundle var2 = new Bundle();
      var2.putParcelable("parent", var1);
      var2.putFloat("angle", this.mAngle);
      var2.putInt("color", this.mCenterOldColor);
      var2.putBoolean("showColor", this.mShowCenterOldColor);
      return var2;
   }

   public boolean onTouchEvent(MotionEvent var1) {
      boolean var5 = false;
      this.getParent().requestDisallowInterceptTouchEvent(true);
      float var3 = var1.getX() - this.mTranslationOffset;
      float var2 = var1.getY() - this.mTranslationOffset;
      switch(var1.getAction()) {
      case 0:
         float[] var6 = this.calculatePointerPosition(this.mAngle);
         if(var3 >= var6[0] - (float)this.mColorPointerHaloRadius && var3 <= var6[0] + (float)this.mColorPointerHaloRadius && var2 >= var6[1] - (float)this.mColorPointerHaloRadius && var2 <= var6[1] + (float)this.mColorPointerHaloRadius) {
            this.mSlopX = var3 - var6[0];
            this.mSlopY = var2 - var6[1];
            this.mUserIsMovingPointer = true;
            this.invalidate();
            break;
         } else {
            if(var3 >= (float)(-this.mColorCenterRadius) && var3 <= (float)this.mColorCenterRadius && var2 >= (float)(-this.mColorCenterRadius) && var2 <= (float)this.mColorCenterRadius && this.mShowCenterOldColor) {
               this.mCenterHaloPaint.setAlpha(80);
               this.setColor(this.getOldCenterColor());
               this.invalidate();
               break;
            }

            this.getParent().requestDisallowInterceptTouchEvent(false);
            return var5;
         }
      case 1:
         this.mUserIsMovingPointer = false;
         this.mCenterHaloPaint.setAlpha(0);
         if(this.onColorSelectedListener != null && this.mCenterNewColor != this.oldSelectedListenerColor) {
            this.onColorSelectedListener.onColorSelected(this.mCenterNewColor);
            this.oldSelectedListenerColor = this.mCenterNewColor;
         }

         this.invalidate();
         break;
      case 2:
         if(!this.mUserIsMovingPointer) {
            this.getParent().requestDisallowInterceptTouchEvent(false);
            return var5;
         }

         this.mAngle = (float)Math.atan2((double)(var2 - this.mSlopY), (double)(var3 - this.mSlopX));
         this.mPointerColor.setColor(this.calculateColor(this.mAngle));
         int var4 = this.calculateColor(this.mAngle);
         this.mCenterNewColor = var4;
         this.setNewCenterColor(var4);
         if(this.mOpacityBar != null) {
            this.mOpacityBar.setColor(this.mColor);
         }

         if(this.mValueBar != null) {
            this.mValueBar.setColor(this.mColor);
         }

         if(this.mSaturationBar != null) {
            this.mSaturationBar.setColor(this.mColor);
         }

         if(this.mSVbar != null) {
            this.mSVbar.setColor(this.mColor);
         }

         this.invalidate();
         break;
      case 3:
         if(this.onColorSelectedListener != null && this.mCenterNewColor != this.oldSelectedListenerColor) {
            this.onColorSelectedListener.onColorSelected(this.mCenterNewColor);
            this.oldSelectedListenerColor = this.mCenterNewColor;
         }
      }

      var5 = true;
      return var5;
   }

   public void setColor(int var1) {
      this.mAngle = this.colorToAngle(var1);
      this.mPointerColor.setColor(this.calculateColor(this.mAngle));
      this.mCenterNewPaint.setColor(this.calculateColor(this.mAngle));
      if(this.mOpacityBar != null) {
         this.mOpacityBar.setColor(this.mColor);
         this.mOpacityBar.setOpacity(Color.alpha(var1));
      }

      if(this.mSVbar != null) {
         Color.colorToHSV(var1, this.mHSV);
         this.mSVbar.setColor(this.mColor);
         if(this.mHSV[1] < this.mHSV[2]) {
            this.mSVbar.setSaturation(this.mHSV[1]);
         } else {
            this.mSVbar.setValue(this.mHSV[2]);
         }
      }

      if(this.mSaturationBar != null) {
         Color.colorToHSV(var1, this.mHSV);
         this.mSaturationBar.setColor(this.mColor);
         this.mSaturationBar.setSaturation(this.mHSV[1]);
      }

      if(this.mValueBar != null && this.mSaturationBar == null) {
         Color.colorToHSV(var1, this.mHSV);
         this.mValueBar.setColor(this.mColor);
         this.mValueBar.setValue(this.mHSV[2]);
      } else if(this.mValueBar != null) {
         Color.colorToHSV(var1, this.mHSV);
         this.mValueBar.setValue(this.mHSV[2]);
      }

      this.invalidate();
   }

   public void setNewCenterColor(int var1) {
      this.mCenterNewColor = var1;
      this.mCenterNewPaint.setColor(var1);
      if(this.mCenterOldColor == 0) {
         this.mCenterOldColor = var1;
         this.mCenterOldPaint.setColor(var1);
      }

      if(this.onColorChangedListener != null && var1 != this.oldChangedListenerColor) {
         this.onColorChangedListener.onColorChanged(var1);
         this.oldChangedListenerColor = var1;
      }

      this.invalidate();
   }

   public void setOldCenterColor(int var1) {
      this.mCenterOldColor = var1;
      this.mCenterOldPaint.setColor(var1);
      this.invalidate();
   }

   public void setOnColorChangedListener(ColorPicker.OnColorChangedListener var1) {
      this.onColorChangedListener = var1;
   }

   public void setOnColorSelectedListener(ColorPicker.OnColorSelectedListener var1) {
      this.onColorSelectedListener = var1;
   }

   public void setShowOldCenterColor(boolean var1) {
      this.mShowCenterOldColor = var1;
      this.invalidate();
   }

   public interface OnColorChangedListener {
      void onColorChanged(int var1);
   }

   public interface OnColorSelectedListener {
      void onColorSelected(int var1);
   }
}
