package com.larswerkman.holocolorpicker;

public final class RR
{
  public RR()
  {
  }

  public static final class array
  {
    public static final int listBrushArray = 2131034112;
    public static final int listBrushValues = 2131034113;
    public static final int listColorArray = 2131034114;
    public static final int listColorValues = 2131034115;

    public array()
    {
    }
  }

  public static final class attr
  {
    public static final int bar_length = 2130771969;
    public static final int bar_orientation_horizontal = 2130771972;
    public static final int bar_pointer_halo_radius = 2130771971;
    public static final int bar_pointer_radius = 2130771970;
    public static final int bar_thickness = 2130771968;
    public static final int color_center_halo_radius = 2130771976;
    public static final int color_center_radius = 2130771975;
    public static final int color_pointer_halo_radius = 2130771978;
    public static final int color_pointer_radius = 2130771977;
    public static final int color_wheel_radius = 2130771973;
    public static final int color_wheel_thickness = 2130771974;

    public attr()
    {
    }
  }

  public static final class dimen
  {
    public static final int bar_length = 2131099648;
    public static final int bar_pointer_halo_radius = 2131099649;
    public static final int bar_pointer_radius = 2131099650;
    public static final int bar_thickness = 2131099651;
    public static final int color_center_halo_radius = 2131099652;
    public static final int color_center_radius = 2131099653;
    public static final int color_pointer_halo_radius = 2131099654;
    public static final int color_pointer_radius = 2131099655;
    public static final int color_wheel_radius = 2131099656;
    public static final int color_wheel_thickness = 2131099657;

    public dimen()
    {
    }
  }

  public static final class drawable
  {
    public static final int app_icon = 2130837504;
    public static final int black = 2130837505;
    public static final int blue = 2130837506;
    public static final int brown = 2130837507;
    public static final int gallery = 2130837508;
    public static final int green = 2130837509;
    public static final int orange = 2130837510;
    public static final int paperclip = 2130837511;
    public static final int pink = 2130837512;
    public static final int red = 2130837513;
    public static final int whatsapp = 2130837514;
    public static final int white = 2130837515;
    public static final int yellow = 2130837516;

    public drawable()
    {
    }
  }

  public static final class id
  {
    public static final int brush_size = 2131296257;
    public static final int color_pick = 2131296259;
    public static final int color_picker = 2131296256;
    public static final int launch_done = 2131296263;
    public static final int main_view = 2131296260;
    public static final int market_button = 2131296261;
    public static final int menu_clear = 2131296266;
    public static final int menu_fg_color = 2131296264;
    public static final int menu_load_image = 2131296267;
    public static final int menu_quit = 2131296269;
    public static final int menu_settings = 2131296268;
    public static final int menu_undo = 2131296265;
    public static final int ok_button = 2131296262;
    public static final int picker = 2131296258;

    public id()
    {
    }
  }

  public static final class layout
  {
    public static final int color = 2130903040;
    public static final int main = 2130903041;

    public layout()
    {
    }
  }

  public static final class menu
  {
    public static final int launch = 2131230720;
    public static final int main = 2131230721;

    public menu()
    {
    }
  }

  public static final class string
  {
    public static final int app_name = 2131165184;
    public static final int color_name = 2131165185;
    public static final int color_pick = 2131165186;
    public static final int intro_market = 2131165187;
    public static final int intro_text_a = 2131165188;
    public static final int intro_text_b = 2131165189;
    public static final int intro_text_c = 2131165190;
    public static final int intro_text_d = 2131165191;
    public static final int launch_done = 2131165192;
    public static final int menu_clear = 2131165193;
    public static final int menu_fg_color = 2131165194;
    public static final int menu_load_image = 2131165195;
    public static final int menu_quit = 2131165196;
    public static final int menu_settings = 2131165197;
    public static final int menu_undo = 2131165198;
    public static final int ok_button = 2131165199;
    public static final int preferences_name = 2131165200;
    public static final int start_info = 2131165201;
    public static final int summary_bg_color = 2131165202;
    public static final int summary_brush_size = 2131165203;
    public static final int summary_fg_color = 2131165204;
    public static final int summary_show_startup_tip = 2131165205;
    public static final int summary_submit_on_back = 2131165206;
    public static final int title_bg_color = 2131165207;
    public static final int title_brush_size = 2131165208;
    public static final int title_fg_color = 2131165209;
    public static final int title_show_startup_tip = 2131165210;
    public static final int title_submit_on_back = 2131165211;
    public static final int undo_not_available = 2131165212;

    public string()
    {
    }
  }

  public static final class styleable
  {
    public static final int[] ColorBars = { 2130771968, 2130771969, 2130771970, 2130771971, 2130771972 };
    public static final int ColorBars_bar_length = 1;
    public static final int ColorBars_bar_orientation_horizontal = 4;
    public static final int ColorBars_bar_pointer_halo_radius = 3;
    public static final int ColorBars_bar_pointer_radius = 2;
    public static final int ColorBars_bar_thickness = 0;
    public static final int[] ColorPicker = { 2130771973, 2130771974, 2130771975, 2130771976, 2130771977, 2130771978 };
    public static final int ColorPicker_color_center_halo_radius = 3;
    public static final int ColorPicker_color_center_radius = 2;
    public static final int ColorPicker_color_pointer_halo_radius = 5;
    public static final int ColorPicker_color_pointer_radius = 4;
    public static final int ColorPicker_color_wheel_radius = 0;
    public static final int ColorPicker_color_wheel_thickness = 1;

    public styleable()
    {
    }
  }

  public static final class xml
  {
    public static final int preferences = 2130968576;

    public xml()
    {
    }
  }
}