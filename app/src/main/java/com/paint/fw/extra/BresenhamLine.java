package com.paint.fw.extra;

import android.graphics.Path;
import android.graphics.Path.Direction;

public class BresenhamLine {

   public void draw(int var1, int var2, int var3, int var4, Path var5, int var6) {
      boolean var11;
      if(Math.abs(var4 - var2) > Math.abs(var3 - var1)) {
         var11 = true;
      } else {
         var11 = false;
      }

      int var10 = var1;
      int var8 = var2;
      int var9 = var3;
      int var7 = var4;
      if(var11) {
         var7 = var3;
         var9 = var4;
         var8 = var1;
         var10 = var2;
      }

      var1 = var10;
      var2 = var8;
      var3 = var9;
      var4 = var7;
      if(var10 > var9) {
         var1 = var9;
         var3 = var10;
         var2 = var7;
         var4 = var8;
      }

      var10 = var3 - var1;
      int var12 = Math.abs(var4 - var2);
      var8 = var10 / 2;
      var7 = var2;
      byte var13;
      if(var2 < var4) {
         var13 = 1;
      } else {
         var13 = -1;
      }

      var4 = var1;

      for(var1 = var8; var4 < var3; var7 = var8) {
         if(var11) {
            var5.addCircle((float)var7, (float)var4, (float)var6, Direction.CCW);
         } else {
            var5.addCircle((float)var4, (float)var7, (float)var6, Direction.CCW);
         }

         var9 = var1 - var12;
         var1 = var9;
         var8 = var7;
         if(var9 < 0) {
            var8 = var7 + var13;
            var1 = var9 + var10;
         }

         ++var4;
      }

   }
}
