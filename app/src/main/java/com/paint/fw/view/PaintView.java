package com.paint.fw.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.Paint.Style;
import android.graphics.Path.Direction;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import com.paint.fw.extra.BresenhamLine;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public final class PaintView extends View {
   private static final String FILENAME = ".paintForWhatsapp";
   private int backgroundColor;
   private Bitmap bitmap = null;
   private BresenhamLine bresenhamLine = null;
   private int brushSize = 8;
   private Canvas canvas = null;
   private Context context = null;
   private int foregroundColor;
   private int height = 0;
   private float lastMouseX = -1.0F;
   private float lastMouseY = -1.0F;
   private ArrayList points = null;
   private SharedPreferences sharedPreferences = null;
   private boolean undoAvailable = false;
   private Bitmap undoBitmap = null;
   private Canvas undoCanvas = null;
   private int width = 0;

   public PaintView(Context var1, int var2, int var3) {
      super(var1);
      this.context = var1;
      this.bresenhamLine = new BresenhamLine();
      this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(var1);
      this.width = var2;
      this.height = var3;
      this.foregroundColor = -16777216;
      if(this.sharedPreferences.getString("fg_color", (String)null) != null) {
         this.foregroundColor = Integer.parseInt(this.sharedPreferences.getString("fg_color", (String)null));
      }

      this.bitmap = Bitmap.createBitmap(var2, var3, Config.ARGB_8888);
      this.canvas = new Canvas(this.bitmap);
      this.clear();
   }

   private void drawLine(MotionEvent var1) {
      Path var2 = new Path();
      if(this.lastMouseX != -1.0F && this.lastMouseX != -1.0F) {
         this.bresenhamLine.draw((int)this.lastMouseX, (int)this.lastMouseY, (int)var1.getX(), (int)var1.getY(), var2, this.brushSize / 2);
         this.canvas.drawPath(var2, this.getPaint());
      } else {
         var2.addCircle(var1.getX(), var1.getY(), (float)(this.brushSize / 2), Direction.CCW);
         this.canvas.drawPath(var2, this.getPaint());
      }

      this.lastMouseX = var1.getX();
      this.lastMouseY = var1.getY();
   }

   private Paint getPaint() {
      Paint var1 = new Paint(1);
      var1.setAntiAlias(true);
      var1.setDither(false);
      var1.setStyle(Style.STROKE);
      var1.setColor(this.foregroundColor);
      var1.setStrokeWidth((float)this.brushSize);
      return var1;
   }

   public void clear() {
      this.backgroundColor = -1;
      if(this.sharedPreferences.getString("bg_color", (String)null) != null) {
         this.backgroundColor = Integer.parseInt(this.sharedPreferences.getString("bg_color", (String)null));
      }

      Paint var1 = new Paint();
      var1.setColor(this.backgroundColor);
      this.canvas.drawPaint(var1);
      this.invalidate();
   }

   public int getForegroundColor() {
      return this.foregroundColor;
   }

   public void loadImage(Uri param1) {
      // $FF: Couldn't be decompiled
   }

   protected void onDraw(Canvas var1) {
      if(this.bitmap != null) {
         var1.drawBitmap(this.bitmap, 0.0F, 0.0F, (Paint)null);
      }

      super.onDraw(var1);
   }

   protected void onSizeChanged(int var1, int var2, int var3, int var4) {
      if(this.bitmap != null) {
         var3 = this.bitmap.getWidth();
      } else {
         var3 = 0;
      }

      if(this.bitmap != null) {
         var4 = this.bitmap.getHeight();
      } else {
         var4 = 0;
      }

      if(var3 < var1 || var4 < var2) {
         int var5 = var3;
         if(var3 < var1) {
            var5 = var1;
         }

         var3 = var4;
         if(var4 < var2) {
            var3 = var2;
         }

         Bitmap var8 = Bitmap.createBitmap(var5, var3, Config.ARGB_8888);
         Canvas var7 = new Canvas();
         var7.setBitmap(var8);
         Paint var6 = new Paint();
         var6.setColor(this.backgroundColor);
         var7.drawPaint(var6);
         if(this.bitmap != null) {
            var7.drawBitmap(this.bitmap, 0.0F, 0.0F, var6);
         }

         this.bitmap = var8;
         this.canvas = var7;
         this.width = var1;
         this.height = var2;
      }

   }

   public boolean onTouchEvent(MotionEvent var1) {
      this.brushSize = 8;
      if(this.sharedPreferences.getString("brush_size", (String)null) != null) {
         this.brushSize = Integer.parseInt(this.sharedPreferences.getString("brush_size", (String)null));
      }

      if(var1.getAction() == 2) {
         this.drawLine(var1);
      } else if(var1.getAction() == 0) {
         this.undoBitmap = Bitmap.createBitmap(this.width, this.height, Config.ARGB_8888);
         this.undoCanvas = new Canvas(this.undoBitmap);
         this.undoCanvas.drawBitmap(this.bitmap, 0.0F, 0.0F, (Paint)null);
         this.undoAvailable = true;
      } else if(var1.getAction() == 1) {
         Log.i(this.getClass().getName(), "ACTION UP ***");
         this.lastMouseX = -1.0F;
         this.lastMouseY = -1.0F;
      }

      this.invalidate();
      return true;
   }

   public void save(Intent var1) throws FileNotFoundException, IOException {
      File var2 = new File(Environment.getExternalStorageDirectory(), ".paintForWhatsapp");
      if(var2 != null) {
         var2.setReadable(true, false);
         var2.setWritable(true, false);
         this.setDrawingCacheEnabled(true);
         this.buildDrawingCache();
         FileOutputStream var3 = new FileOutputStream(var2);
         this.getDrawingCache().compress(CompressFormat.JPEG, 90, var3);
         var3.flush();
         var3.close();
         var1.setDataAndType(Uri.fromFile(var2), "image/jpeg");
      }

   }

   public void setBackground(int var1) {
      this.backgroundColor = var1;
   }

   public void setBrushSize(int var1) {
      this.brushSize = var1;
   }

   public void setForeground(int var1) {
      this.foregroundColor = var1;
   }

   public boolean undo() {
      boolean var1 = false;
      if(this.undoAvailable) {
         Log.i(this.getClass().getName(), "UNDOING ***");
         this.bitmap = this.undoBitmap;
         this.canvas = this.undoCanvas;
         this.invalidate();
         this.undoAvailable = false;
         var1 = true;
      }

      return var1;
   }
}
