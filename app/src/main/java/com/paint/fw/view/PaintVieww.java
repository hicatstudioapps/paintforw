package com.paint.fw.view;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.Paint.Style;
import android.graphics.Path.Direction;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.paint.fw.DeviceTool;
import com.paint.fw.extra.BresenhamLine;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public final class PaintVieww extends View {


   private static final String FILENAME = ".paintForWhatsapp";
   private int backgroundColor;
   private Bitmap bitmap = null;
   private BresenhamLine bresenhamLine = null;
   private int brushSize = 8;
   private Canvas canvas = null;
   private Context context = null;
   private int foregroundColor;
   private int height = 0;
   private float lastMouseX = -1.0F;
   private float lastMouseY = -1.0F;
   private ArrayList points = null;
   private SharedPreferences sharedPreferences = null;
   private boolean undoAvailable = false;
   private Bitmap undoBitmap = null;
   private Canvas undoCanvas = null;
   private int width = 0;

   public void init(int var2, int var3) {
//      super(var1);
//      this.context = var1;
      this.bresenhamLine = new BresenhamLine();
      this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
      this.width = var2;
      this.height = var3;
      this.foregroundColor = -16777216;
      if(this.sharedPreferences.getString("fg_color", (String)null) != null) {
         this.foregroundColor = Integer.parseInt(this.sharedPreferences.getString("fg_color", (String)null));
      }
      this.bitmap = Bitmap.createBitmap(var2, var3, Config.ARGB_8888);
      this.canvas = new Canvas(this.bitmap);
      this.clear();
   }

   @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
   public PaintVieww(Context context, AttributeSet attrs) {
      super(context, attrs);
//      this.width = width;
      this.context = context;
//      this.bresenhamLine = new BresenhamLine();
//      this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//      DisplayMetrics localDisplayMetrics = new DisplayMetrics();
//      getDisplay().getMetrics(localDisplayMetrics);
//      this.width = localDisplayMetrics.widthPixels;
//      this.height = localDisplayMetrics.heightPixels;
//      this.foregroundColor = -16777216;
//      if(this.sharedPreferences.getString("fg_color", (String)null) != null) {
//         this.foregroundColor = Integer.parseInt(this.sharedPreferences.getString("fg_color", (String)null));
//      }
//      this.bitmap = Bitmap.createBitmap(this.width, this.height, Config.ARGB_8888);
//      this.canvas = new Canvas(this.bitmap);
//      this.clear();
   }

   private void drawLine(MotionEvent var1) {
      Path var2 = new Path();
      if(this.lastMouseX != -1.0F && this.lastMouseX != -1.0F) {
         this.bresenhamLine.draw((int)this.lastMouseX, (int)this.lastMouseY, (int)var1.getX(), (int)var1.getY(), var2, this.brushSize / 2);
         this.canvas.drawPath(var2, this.getPaint());
      } else {
         var2.addCircle(var1.getX(), var1.getY(), (float)(this.brushSize / 2), Direction.CCW);
         this.canvas.drawPath(var2, this.getPaint());
      }

      this.lastMouseX = var1.getX();
      this.lastMouseY = var1.getY();
   }

   private Paint getPaint() {
      Paint var1 = new Paint(1);
      var1.setAntiAlias(true);
      var1.setDither(false);
      var1.setStyle(Style.STROKE);
      var1.setColor(this.foregroundColor);
      var1.setStrokeWidth((float)this.brushSize);
      return var1;
   }

   public void clear() {
      this.backgroundColor = -1;
      if(this.sharedPreferences.getString("bg_color", (String)null) != null) {
         this.backgroundColor = Integer.parseInt(this.sharedPreferences.getString("bg_color", (String)null));
      }

      Paint var1 = new Paint();
      var1.setColor(this.backgroundColor);
      this.canvas.drawPaint(var1);
      this.invalidate();
   }

   public int getForegroundColor() {
      return this.foregroundColor;
   }

   protected Bitmap getBitmap(Uri uri, Context resolver) {
      BitmapFactory.Options options;
      int i;
      InputStream inputstream1;
      Bitmap bitmap;
      Bitmap bitmap1;
      ExifInterface exifinterface;
      ExifInterface exifinterface1;
      Bitmap bitmap2;
      int j;
      BitmapFactory.Options options1;
      Bitmap bitmap3;
      int k;
      int l;
      double d;
      Bitmap bitmap4;
      try {

         InputStream inputstream = resolver.getContentResolver().openInputStream(uri);
         options = new BitmapFactory.Options();
          //options.outHeight=this.height;
         //options.inJustDecodeBounds = true;
         // options.outWidth=width;
          Bitmap b=  BitmapFactory.decodeStream(inputstream, null, options);
          int reqWidth=0, reqHeight=0;
         inputstream.close();
          if(width<= b.getWidth()){
              reqWidth=width;
          }else if(width > b.getWidth()){
              reqWidth=b.getWidth();
          }
          if(height<= b.getHeight()){
              reqHeight=height;
          }else if(height > b.getHeight()){
              reqHeight=b.getHeight();
          }
         return Bitmap.createScaledBitmap(b,reqWidth,reqHeight,true);
//         i = 1;
//         while ((double) (options.outWidth * options.outHeight) * (1.0D / Math.pow(i, 2D)) > (double) 1024) {
//            i++;
//         }
//         inputstream1 = resolver.getContentResolver().openInputStream(uri);
//         if (i <= 1) {
//            bitmap = BitmapFactory.decodeStream(inputstream1);
//            bitmap1 = bitmap;
//         } else {
//            j = i - 1;
//            options1 = new BitmapFactory.Options();
//            options1.inSampleSize = j;
//            bitmap3 = BitmapFactory.decodeStream(inputstream1, null, options1);
//            k = bitmap3.getHeight();
//            l = bitmap3.getWidth();
//            d = Math.sqrt((double) 1024 / ((double) l / (double) k));
//            //bitmap4 = Bitmap.createScaledBitmap(bitmap3, (int) ((d / (double) k) * (double) l), (int) d, true);
//          //  bitmap3.recycle();
//            bitmap1 = bitmap3;
//            System.gc();
//         }
//         inputstream1.close();
//         if (android.os.Build.VERSION.SDK_INT < 19) {
//            exifinterface = new ExifInterface(DeviceTool.getRealImagePathFromURI(resolver, uri));
//            exifinterface1 = exifinterface;
//         } else
//            exifinterface1 = new ExifInterface(DeviceTool.getRealImagePathFromURIKITKAT(resolver, uri));
//         bitmap2 = DeviceTool.rotateBitmap(bitmap1, exifinterface1.getAttributeInt("Orientation", 0));
//         return bitmap2;
      } catch (Exception exception) {
         return null;
      }
   }

   public void loadImage(Uri param1, Context param2) {
      // $FF: Couldn't be decompiled
       Bitmap bit=getBitmap(param1,param2);
       clear();
       this.canvas.drawBitmap(bit, ((width/2)-(bit.getWidth()/2)),((height/2)-(bit.getHeight()/2)), (Paint)null);
//       this.canvas.setBitmap(bit);
      invalidate();
      int x=0;
      int y=5+5;
   }

   protected void onDraw(Canvas var1) {
      if(this.bitmap != null) {
         var1.drawBitmap(this.bitmap, 0.0F, 0.0F, (Paint)null);
      }
//      int alto= getMeasuredHeight();
//      int ancho= getMeasuredWidth();
//      Paint p=new Paint();
//      p.setStyle(Style.FILL);
//      p.setColor(getResources().getColor(android.support.design.R.color.material_blue_grey_800));
//      var1.drawRect(0,0,alto,ancho,p);
      super.onDraw(var1);
   }

   @Override
   protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
      int ancho = calcularAncho(widthMeasureSpec);
      int alto = calcularAlto(heightMeasureSpec);

      setMeasuredDimension(ancho, alto);
   }

   private int calcularAlto(int heightMeasureSpec) {

      int res = 100; //Alto por defecto
      int modo = MeasureSpec.getMode(heightMeasureSpec);
      int limite = MeasureSpec.getSize(heightMeasureSpec);
      if (modo == MeasureSpec.AT_MOST) {
         res = limite;
      }
      else if (modo == MeasureSpec.EXACTLY) {
         res = limite;
      }
      return res;
   }

   private int calcularAncho(int widthMeasureSpec) {
      int res = 200; //Ancho por defecto
      int modo = MeasureSpec.getMode(widthMeasureSpec);
      int limite = MeasureSpec.getSize(widthMeasureSpec);
      if (modo == MeasureSpec.AT_MOST) {
         res = limite;
      }
      else if (modo == MeasureSpec.EXACTLY) {
         res = limite;
      }
      return res;
   }

   protected void onSizeChanged(int var1, int var2, int var3, int var4) {
      if(this.bitmap != null) {
         var3 = this.bitmap.getWidth();
      } else {
         var3 = 0;
      }

      if(this.bitmap != null) {
         var4 = this.bitmap.getHeight();
      } else {
         var4 = 0;
      }

      if(var3 < var1 || var4 < var2) {
         int var5 = var3;
         if(var3 < var1) {
            var5 = var1;
         }

         var3 = var4;
         if(var4 < var2) {
            var3 = var2;
         }

         Bitmap var8 = Bitmap.createBitmap(var5, var3, Config.ARGB_8888);
         Canvas var7 = new Canvas();
         var7.setBitmap(var8);
         Paint var6 = new Paint();
         var6.setColor(this.backgroundColor);
         var7.drawPaint(var6);
         if(this.bitmap != null) {
            var7.drawBitmap(this.bitmap, 0.0F, 0.0F, var6);
         }

         this.bitmap = var8;
         this.canvas = var7;
         this.width = var1;
         this.height = var2;
      }

   }

   public boolean onTouchEvent(MotionEvent var1) {
      this.brushSize = 8;
      if(this.sharedPreferences.getString("brush_size", (String)null) != null) {
         this.brushSize = Integer.parseInt(this.sharedPreferences.getString("brush_size", (String)null));
      }

      if(var1.getAction() == 2) {
         this.drawLine(var1);
      } else if(var1.getAction() == 0) {
         this.undoBitmap = Bitmap.createBitmap(this.width, this.height, Config.ARGB_8888);
         this.undoCanvas = new Canvas(this.undoBitmap);
         this.undoCanvas.drawBitmap(this.bitmap, 0.0F, 0.0F, (Paint)null);
         this.undoAvailable = true;
      } else if(var1.getAction() == 1) {
         Log.i(this.getClass().getName(), "ACTION UP ***");
         this.lastMouseX = -1.0F;
         this.lastMouseY = -1.0F;
      }

      this.invalidate();
      return true;
   }

   public void save(Intent var1) throws FileNotFoundException, IOException {
      File var2 = new File(Environment.getExternalStorageDirectory(), ".paintForWhatsapp");
      if(var2 != null) {
         var2.setReadable(true, false);
         var2.setWritable(true, false);
         this.setDrawingCacheEnabled(true);
         this.buildDrawingCache();
         FileOutputStream var3 = new FileOutputStream(var2);
         this.getDrawingCache().compress(CompressFormat.JPEG, 90, var3);
         var3.flush();
         var3.close();
         var1.setDataAndType(Uri.fromFile(var2), "image/jpeg");
      }

   }

   public void setBackground(int var1) {
      this.backgroundColor = var1;
   }

   public void setBrushSize(int var1) {
      this.brushSize = var1;
   }

   public void setForeground(int var1) {
      this.foregroundColor = var1;
   }

   public boolean undo() {
      boolean var1 = false;
      if(this.undoAvailable) {
         Log.i(this.getClass().getName(), "UNDOING ***");
         this.bitmap = this.undoBitmap;
         this.canvas = this.undoCanvas;
         this.invalidate();
         this.undoAvailable = false;
         var1 = true;
      }

      return var1;
   }
}
