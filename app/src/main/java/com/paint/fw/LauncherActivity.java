package com.paint.fw;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class LauncherActivity extends Activity {

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
       getWindow().addFlags(Window.FEATURE_NO_TITLE);
      this.setContentView(R.layout.main);
      ((TextView)findViewById(R.id.a)).setTypeface(SpotTheCatApp.face);
      ((TextView)findViewById(R.id.b)).setTypeface(SpotTheCatApp.face);
      ((TextView)findViewById(R.id.c)).setTypeface(SpotTheCatApp.face);
      ((TextView)findViewById(R.id.d)).setTypeface(SpotTheCatApp.face);

      ((Button)this.findViewById(R.id.ok_button)).setTypeface(SpotTheCatApp.face);
      ((Button)this.findViewById(R.id.ok_button)).setOnClickListener(new OnClickListener() {
         public void onClick(View var1) {
            LauncherActivity.this.finish();
            SpotTheCatApp.showInterstitial();
         }
      });
      ((Button)this.findViewById(R.id.market_button)).setTypeface(SpotTheCatApp.face);
      ((Button)this.findViewById(R.id.market_button)).setOnClickListener(new OnClickListener() {
         public void onClick(View var1) {
            Intent var2 = new Intent("android.intent.action.VIEW");
            var2.setData(Uri.parse("market://details?id=com.whatsapp"));
            LauncherActivity.this.startActivity(var2);
         }
      });
   }

   public boolean onCreateOptionsMenu(Menu var1) {
      this.getMenuInflater().inflate(R.menu.launch, var1);
      return true;
   }

   public boolean onOptionsItemSelected(MenuItem var1) {
      switch(var1.getItemId()) {
      case 2131296263:
         this.finish();
      default:
         return super.onOptionsItemSelected(var1);
      }
   }
}
