package com.paint.fw;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;
import com.larswerkman.holocolorpicker.ColorPicker;

public class ColorActivity extends AppCompatActivity {
   public static final String PICKED_COLOR = "PickedColor";
   private Intent intent;
   private ColorPicker picker;

   private void done() {
      this.setResult(-1, this.intent);
      this.finish();
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2130903040);
      final SharedPreferences var4 = PreferenceManager.getDefaultSharedPreferences(this.getBaseContext());
      final SeekBar var3 = (SeekBar)this.findViewById(2131296257);
      var3.setProgress(Integer.parseInt(var4.getString("brush_size", "8")));
      this.picker = (ColorPicker)this.findViewById(2131296258);
      Button var2 = (Button)this.findViewById(2131296259);
      this.intent = this.getIntent();
      this.picker.setOldCenterColor(this.intent.getIntExtra("oldColor", -16777216));
      var2.setOnClickListener(new OnClickListener() {
         public void onClick(View var1) {
            var4.edit().putString("brush_size", String.valueOf(var3.getProgress())).apply();
            ColorActivity.this.intent.putExtra("PickedColor", ColorActivity.this.picker.getColor());
            ColorActivity.this.done();
         }
      });
   }
}
