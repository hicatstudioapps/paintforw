package com.paint.fw;

import android.content.*;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Environment;
import android.os.StatFs;
import android.provider.DocumentsContract;
import java.io.File;

public class DeviceTool
{

    public DeviceTool()
    {
    }

    public static boolean ExistSDCard()
    {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static String getDataColumn(Context context, Uri uri, String s, String as[])
    {
        Cursor cursor;
        String as1[];
        try{
	        cursor = null;
	        as1 = (new String[] {
	            "_data"
	        });
	        cursor = context.getContentResolver().query(uri, as1, s, as, null);
	        if(cursor == null)
	        {
	        	return null;
	        }
	        String s1;
	        if(!cursor.moveToFirst())
	        {
	            return null;
	        }
	        s1 = cursor.getString(cursor.getColumnIndexOrThrow("_data"));
	        if(cursor != null)
	        {
	            cursor.close();
	        }
	        return s1;
        }catch(Exception e){
        	return null;
        }
    }

    public static String getRealImagePathFromURI(Context context, Uri uri)
    {
        Cursor cursor = null;
        String s;
        String as[] = {
            "_data"
        };
        try{
	        cursor = context.getContentResolver().query(uri, as, null, null, null);
	        int i = cursor.getColumnIndexOrThrow("_data");
	        cursor.moveToFirst();
	        s = cursor.getString(i);
	        if(cursor != null)
	        {
	            cursor.close();
	        }
	        return s;
        }catch(Exception e){
        	return null;
        }
    }

    public static String getRealImagePathFromURIKITKAT(Context context, Uri uri)
    {
        boolean flag;
        String s;
        String as[];
        String s1;
        if(android.os.Build.VERSION.SDK_INT >= 19)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        if(!flag || !DocumentsContract.isDocumentUri(context, uri)){
        	if("content".equalsIgnoreCase(uri.getScheme()))
            {
                return getDataColumn(context, uri, null, null);
            }
            boolean flag1 = "file".equalsIgnoreCase(uri.getScheme());
            s = null;
            if(flag1)
            {
                return uri.getPath();
            }
            return s;
        }
        if(!isExternalStorageDocument(uri)){
        	if(isDownloadsDocument(uri))
            {
                String s2 = DocumentsContract.getDocumentId(uri);
                return getDataColumn(context, ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(s2).longValue()), null, null);
            }
            boolean flag2 = isMediaDocument(uri);
            s = null;
            if(!flag2)
            {
            	if("content".equalsIgnoreCase(uri.getScheme()))
                {
                    return getDataColumn(context, uri, null, null);
                }
                boolean flag1 = "file".equalsIgnoreCase(uri.getScheme());
                s = null;
                if(flag1)
                {
                    return uri.getPath();
                }
                return s;
            }
            as = DocumentsContract.getDocumentId(uri).split(":");
            s1 = as[0];
            Uri uri1;
            if(!"image".equals(s1)){
            	if("video".equals(s1))
                {
                    uri1 = android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else {
                    boolean flag3 = "audio".equals(s1);
                    uri1 = null;
                    if(flag3)
                    {
                        uri1 = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }
                }
            }else
            	uri1 = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            String as1[] = new String[1];
            as1[0] = as[1];
            return getDataColumn(context, uri1, "_id=?", as1);
        }else{
	        String as2[] = DocumentsContract.getDocumentId(uri).split(":");
	        boolean flag4 = "primary".equalsIgnoreCase(as2[0]);
	        s = null;
	        if(flag4)
	        {
	            s = (new StringBuilder()).append(Environment.getExternalStorageDirectory()).append("/").append(as2[1]).toString();
	        }
        }
        return s;
    }

    public static boolean isDownloadsDocument(Uri uri)
    {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isExternalStorageDocument(Uri uri)
    {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri)
    {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static Bitmap rotateBitmap(Bitmap bitmap, int i)
    {
        Matrix matrix = new Matrix();
        switch(i){
        default:case 1:
        	return bitmap;
        case 2:
        	matrix.setScale(-1F, 1.0F);
        	break;
        case 3:
        	matrix.setRotate(180F);
        	break;
        case 4:
        	matrix.setRotate(180F);
            matrix.postScale(-1F, 1.0F);
            break;
        case 5:
        	matrix.setRotate(90F);
            matrix.postScale(-1F, 1.0F);
            break;
        case 6:
        	matrix.setRotate(90F);
        	break;
        case 7:
        	matrix.setRotate(-90F);
            matrix.postScale(-1F, 1.0F);
            break;
        case 8:
        	matrix.setRotate(-90F);
        	break;
        }
        Bitmap bitmap1;
        try
        {
            bitmap1 = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            bitmap.recycle();
        }
        catch(OutOfMemoryError outofmemoryerror)
        {
            outofmemoryerror.printStackTrace();
            return null;
        }
        return bitmap1;
    }

    public long getSDAllSize()
    {
        StatFs statfs = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return ((long)statfs.getBlockSize() * (long)statfs.getBlockCount()) / 1024L / 1024L;
    }

    public long getSDFreeSize()
    {
        StatFs statfs = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return ((long)statfs.getBlockSize() * (long)statfs.getAvailableBlocks()) / 1024L / 1024L;
    }
}
