package com.paint.fw;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.larswerkman.holocolorpicker.ColorPicker;
import com.paint.fw.view.PaintVieww;
import java.io.FileNotFoundException;
import java.io.IOException;

import badabing.lib.ServerUtilities;

public class PaintActivity extends AppCompatActivity implements OnClickListener
{
  private int height = 0;
  private PaintVieww paintView = null;
  private SharedPreferences sharedPreferences;
  private int width = 0;
  private Toolbar toolbar;
    private RelativeLayout menu;
    private Animation inA;
    private Animation outA;
    public static final String PICKED_COLOR = "PickedColor";
    private Intent intent;
    private ColorPicker picker;
    private View colorPicker;
    private boolean colorPickerOpen;
    private AdView mAdView;

    @Override
    public void onBackPressed() {
        if(menuOpen){
            menuOpen=false;
            menu.setAnimation(outA);
            menu.setVisibility(View.GONE);
        }
        else if(colorPickerOpen){
            colorPicker.setAnimation(AnimationUtils.makeOutAnimation(this,true));
            colorPicker.setVisibility(View.GONE);
            colorPickerOpen=false;
        }else
            super.onBackPressed();
    }

    private boolean menuOpen;

    public PaintActivity()
  {
  }

  private void done()
  {
    Intent localIntent = getIntent();
    try
    {
      this.paintView.save(localIntent);
      setResult(-1, localIntent);
      finish();
      return;
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      while (true)
        Log.e(PaintActivity.class.getName(), localFileNotFoundException.getMessage());
    }
    catch (IOException localIOException)
    {
      while (true)
        Log.e(PaintActivity.class.getName(), localIOException.getMessage());
    }
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if ((paramInt1 == 2) && (paramInt2 == -1))
      this.paintView.setForeground(paramIntent.getIntExtra("PickedColor", 0));
    if ((paramInt1 == 3) && (paramInt2 == -1))
      this.paintView.loadImage(paramIntent.getData(),this);
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
    this.height = localDisplayMetrics.heightPixels;
    this.width = localDisplayMetrics.widthPixels;
    setContentView(R.layout.activity_paint);
      ServerUtilities.registerWithGCM(this);
      mAdView = (AdView)findViewById(R.id.adView);
      AdRequest adRequest = new AdRequest.Builder().build();
      mAdView.setAdListener(new AdListener() {
          @Override
          public void onAdLoaded() {
              if (mAdView.getVisibility() != View.VISIBLE) {
                  mAdView.setVisibility(View.VISIBLE);
              }
          }
      });
      // Start loading the ad in the background.
      mAdView.loadAd(adRequest);
      menu=(RelativeLayout)findViewById(R.id.menu);
      findViewById(R.id.clear_screen).setOnClickListener(this);
      findViewById(R.id.preference).setOnClickListener(this);
      findViewById(R.id.done).setOnClickListener(this);
      findViewById(R.id.import_picture).setOnClickListener(this);
    inA= AnimationUtils.loadAnimation(this,R.anim.abc_fade_in);
      menu.setAnimation(inA);
    toolbar=(Toolbar)findViewById(R.id.toolbar);
    toolbar.setNavigationIcon(R.drawable.ic_menu_more);
      toolbar.setTitle("");
      setSupportActionBar(toolbar);
      toolbar.setNavigationOnClickListener(new OnClickListener() {
          @Override
          public void onClick(View v) {
              if(menuOpen){
                  menuOpen=false;
                  menu.setAnimation(outA);
                  menu.setVisibility(View.GONE);
              }else{
                  if(colorPickerOpen){
                      colorPicker.setAnimation(AnimationUtils.makeOutAnimation(getApplicationContext(),true));
                      colorPicker.setVisibility(View.GONE);
                      colorPickerOpen=false;
                  }
              menuOpen=true;
                  menu.setAnimation(inA);
//              Animation menuA = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.abc_slide_in_top);
//              menu.setAnimation(menuA);
              menu.setVisibility(View.VISIBLE);}
          }
      });
    this.paintView=(PaintVieww)findViewById(R.id.view);

    int temp= (int) getResources().getDimension(android.support.design.R.dimen.abc_action_bar_default_height_material);
    this.paintView.init(width,height-100);
    //this.paintView = new PaintVieww(this, this.width, this.height);
    this.paintView.requestFocus();
    this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    if (this.sharedPreferences.getBoolean("show_startup_tip", true))
      Toast.makeText(this, R.string.start_info, Toast.LENGTH_SHORT).show();
      final SharedPreferences var4 = PreferenceManager.getDefaultSharedPreferences(this.getBaseContext());
      final SeekBar var3 = (SeekBar)this.findViewById(R.id.brush_size);
      var3.setProgress(Integer.parseInt(var4.getString("brush_size", "8")));
      this.picker = (ColorPicker)this.findViewById(R.id.picker);
      Button var2 = (Button)this.findViewById(R.id.color_pick);
      //this.intent = this.getIntent();
      this.picker.setOldCenterColor(this.paintView.getForegroundColor());
      var2.setOnClickListener(new OnClickListener() {
          public void onClick(View var1) {
              var4.edit().putString("brush_size", String.valueOf(var3.getProgress())).apply();
              paintView.setForeground(picker.getColor());
              colorPickerOpen=false;
              colorPicker.setAnimation(AnimationUtils.makeOutAnimation(getApplicationContext(),true));
              colorPicker.setVisibility(View.GONE);
          }
      });
      colorPicker=findViewById(R.id.color_picker);
      colorPickerOpen=false;
      menuOpen=false;
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(R.menu.main, paramMenu);
    return true;
  }

//  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
//  {
//    if (paramInt == 4)
//    {
//      if (!this.sharedPreferences.getBoolean("submit_on_back", true))
//          setResult(0);
//      done();
//    }
//
//      return super.onKeyDown(paramInt, paramKeyEvent);
//  }

    @Override
    protected void onDestroy() {
        super.onStop();
        if(mAdView !=null)
            mAdView.destroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mAdView !=null)
            mAdView.resume();
    }

    public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
//    case R.id.menu_clear:
//        this.paintView.clear();
//        break;
    case R.id.menu_undo:
        this.paintView.undo();
        break;
//    case R.id.menu_quit:
//        done();
//        break;
    case R.id.menu_fg_color:
        if(!colorPickerOpen){
            colorPicker.setAnimation(AnimationUtils.makeInAnimation(this,false));
            colorPicker.setVisibility(View.VISIBLE);
        colorPickerOpen=true;}
//        Intent localIntent2 = new Intent(this, ColorActivity.class);
//        localIntent2.putExtra("oldColor", this.paintView.getForegroundColor());
//        startActivityForResult(localIntent2, 2);

        break;
//    case R.id.menu_settings:
//        startActivity(new Intent(this, PreferencesActivity.class));
//        break;
    case R.string.undo_not_available:
        Toast.makeText(this, R.string.undo_not_available, Toast.LENGTH_SHORT).show();
        break;
        case R.id.menu_close:
            finish();
            break;
//    case R.id.menu_load_image:
//        Intent localIntent1 = new Intent("android.intent.action.GET_CONTENT");
//        localIntent1.setType("image/*");
//        startActivityForResult(localIntent1, 3);
//        break;
    }
      return true;
//    while (true)
//    {
//      //return super.onOptionsItemSelected(paramMenuItem);
////      this.paintView.clear();
//     // continue;
//      if (!this.paintView.undo())
//      {
////        Toast.makeText(this, R.string.undo_not_available, Toast.LENGTH_SHORT).show();
//      //  continue;
////        done();
//      //  continue;
////        Intent localIntent2 = new Intent(this, ColorActivity.class);
////        localIntent2.putExtra("oldColor", this.paintView.getForegroundColor());
////        startActivityForResult(localIntent2, R.id.menu_fg_color);
//        //continue;
////        startActivity(new Intent(this, PreferencesActivity.class));
//       // continue;
////        Intent localIntent1 = new Intent("android.intent.action.GET_CONTENT");
////        localIntent1.setType("image/*");
////        startActivityForResult(localIntent1, R.id.menu_load_image);
//      }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.clear_screen:
                this.paintView.clear();
                menu.setVisibility(View.GONE);
                break;
            case R.id.preference:
                menu.setVisibility(View.GONE);
                startActivity(new Intent(this, PreferencesActivity.class));
                break;
            case R.id.done:
                done();
                SpotTheCatApp.showInterstitial();
                break;
            case R.id.import_picture:
                menu.setVisibility(View.GONE);
                Intent localIntent1 = new Intent("android.intent.action.GET_CONTENT");
                localIntent1.setType("image/*");
                startActivityForResult(localIntent1, 3);
                break;
        }
    }
}